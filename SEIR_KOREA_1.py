import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
"""
COVID-19 South Korea

Initialised values here: 
18th of Februrary onwards
Sample Size N = 51,844,627
Susceptible S = 51,843,681
Exposed E = 31
Infected I = 893
Recovered = 22
Incubation time, Sigma = 1/5.2 = 0.1923
Basic reproductive number = 2.3
Effective reproductive number 3.96 = S(t)/N * R0 = 896/51Mil * 2.3 
Infection rate, Beta β = 0.799
Recovery rate, Gamma γ = 0.202

"""

def SEIR_model(y, t, N, beta, gamma, alpha):

    S, E, I, R = y
    dS_dt = -beta * S * I / N
    dE_dt = beta * S * I / N - (alpha * E)
    dI_dt = (alpha * E) - (gamma * I)
    dR_dt = gamma * I

    return [dS_dt, dE_dt, dI_dt, dR_dt]
#N population
N0 = 51844627

#S,E,I,R = 51843681, 31, 893, 22
S,E,I,R = 51843681, 31, 893, 22
#infection rate ß
beta = 0.799
#recovery rate gamma
gamma = 0.202
# incubation rate alpha
alpha = 0.1923

# time points , week number shown on x-axis
t = np.linspace(0, 100, 1000)

# ordinary differential equations, study source: https://apmonitor.com/pdc/index.php/Main/SolveDifferentialEquations
solution = odeint(SEIR_model, [S,E,I,R], t, args=(N0, beta, gamma, alpha))

#customising y-axis to milion scale
def millions(x, pos):
    'The two args are the value and tick position'
    return '%1.1fM' % (x * 1e-6)

formatter = FuncFormatter(millions)


# plotting the data on three separate curves for S,I,R
fig= plt.figure()
ax = fig.add_subplot()
ax.plot(t, solution[:, 0], 'b', lw=1, label="Susceptible")
ax.plot(t, solution[:, 1], 'y', lw=1, label="Exposed")
ax.plot(t, solution[:, 2], 'r', lw=1, label="Infected")
ax.plot(t, solution[:, 3], 'g', lw=1, label="Recovered")
ax.yaxis.set_major_formatter(formatter)
#plotting formula found here: https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.odeint.html
plt.suptitle("COVID-19, South Korea", fontsize=14)
plt.title("Scenario 2 without the intervention")
plt.ylim(0, 55000000)
plt.xlabel("Time /days")
plt.ylabel('Population')
plt.legend(loc='best')
plt.grid()
plt.show() #display the figure

