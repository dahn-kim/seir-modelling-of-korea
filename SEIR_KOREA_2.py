import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
"""
COVID-19 South Korea 

Initialised values here: 
Based on statistics between 19.01.20 - 18.02.29
Sample Size N = 51,844,627
Susceptible S = 51844581
Exposed E = 3
Infected I = 31
Recovered = 12
Incubation time, Sigma = 1/5.2 = 0.1923
Infection rate, Beta β = 0.276
Recovery rate, Gamma γ =0.202
Basic Reproductive Number = 2.3
Effective Reproductive Number = 1.37

Further reading
1. Scipy.org - https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.odeint.html
2. Dynamics and Control - https://apmonitor.com/pdc/index.php/Main/SolveDifferentialEquations
"""

def SEIR_model(y, t, N, beta, gamma, alpha):

    S, E, I, R = y
    dS_dt = -beta * S * I / N
    dE_dt = beta * S * I / N - (alpha * E)
    dI_dt = (alpha * E) - (gamma * I)
    dR_dt = gamma * I

    return [dS_dt, dE_dt, dI_dt, dR_dt]
#N population
N0 = 51844627

#S,E,I,R = 51844581, 3, 31, 12
S,E,I,R = 51844581, 3, 31, 12

#infection rate
beta = 0.276
#recovery rate gamma
gamma = 0.202
# incubation rate alpha
alpha = 0.1923

# time points , week number shown on x-axis
t = np.linspace(0, 1000, 1000)

# ordinary differential equations, study source: https://apmonitor.com/pdc/index.php/Main/SolveDifferentialEquations
solution = odeint(SEIR_model, [S,E,I,R], t, args=(N0, beta, gamma, alpha))

#customising y-axis to milion scale
def millions(x, pos):
    'The two args are the value and tick position'
    return '%1.1fM' % (x * 1e-6)

formatter = FuncFormatter(millions)


# plotting the data on three separate curves for S,I,R
fig= plt.figure()
ax = fig.add_subplot()
ax.plot(t, solution[:, 0], 'b', lw=1, label="Susceptible")
ax.plot(t, solution[:, 1], 'y', lw=1, label="Exposed")
ax.plot(t, solution[:, 2], 'r', lw=1, label="Infected")
ax.plot(t, solution[:, 3], 'g', lw=1, label="Recovered")
ax.yaxis.set_major_formatter(formatter)
#plotting formula found here: https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.odeint.html
plt.suptitle("COVID-19, South Korea", fontsize=14)
plt.title("Scenario 1 with the intervention")
#plt.ylim(0, 55000000)
plt.xlabel("Time /days")
plt.ylabel('Population')
plt.legend(loc='best')
plt.grid()
plt.show() #display the figure


