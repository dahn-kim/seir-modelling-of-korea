# SEIR modelling of Korea

<p>S.E.I.R modelling (Susceptible-Exposed-Infected-Recovered) modelling simulation of COVID-19 in South Korea based on two imaginary scenarios:</p> 

<p>
Scenario 1: COVID-19 prediction of South Korea based on the statistics by 18th of February and the input of intervention (self-isolation, social distancing and mask wearing)

</p>

<p>
Scenario 2: COVID-19 prediction of South Korea based on the statistics from 18th of February with the mass outbreak scandal by a religious community, no intervention added
</p>